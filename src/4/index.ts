// https://adventofcode.com/2022/day/4

import { loadInputFile_StrByLine } from "../utils";

const raw_input = loadInputFile_StrByLine(__dirname, 'input.txt');
const pairs = raw_input.map(l => l.split(',', 2).map(p => p.split('-',2).map(n => parseInt(n, 10))))

// console.log(JSON.stringify(pairs));

function fullyIncludes(pair1: number[], pair2: number[]): boolean {
  return (pair1[0] <= pair2[0]) && (pair1[1] >= pair2[1]);
}

function overlaps(pair1: number[], pair2: number[]): boolean {
  return (pair1[0] <= pair2[1]) && (pair1[1] >= pair2[0]);
}

const fully_including_pairs = pairs.filter(p => {
  const [elf1, elf2] = p;
  return fullyIncludes(elf1, elf2) || fullyIncludes(elf2, elf1)
})

// console.log('Overlapping pairs: ', JSON.stringify(overlapping_pairs));
console.log('Part 1: In how many assignment pairs does one range fully contain the other?', fully_including_pairs.length);

const overlapping_pairs = pairs.filter(p => {
  const [elf1, elf2] = p;
  return overlaps(elf1, elf2)
})

// console.log('Overlapping pairs: ', JSON.stringify(overlapping_pairs));
console.log('Part 2: In how many assignment pairs do the ranges overlap?', overlapping_pairs.length);
