// https://adventofcode.com/2022/day/11

import { assert } from "console";
import { loadInputFile_StrByLine, Point } from "../utils";

interface Reducible {
    getRemainder(base: number): number;
    // print(): string;
}
enum OperationType {
    Multiply,
    Add,
    Square,
}
type Operation = {
    type: OperationType,
    value: number,
};

class OperationStack implements Reducible {
    public operations = new Array<Operation>();
    constructor(public startNumber: number) {}
    getRemainder(base: number): number {
        return this.operations.reduce((prev, curr) => {
            switch (curr.type) {
                case OperationType.Multiply:
                    return  prev * (curr.value % base);
                case OperationType.Add:
                    return  (prev + curr.value) % base;
                case OperationType.Square:
                    return (prev * prev) % base;
            }
        }, this.startNumber);
    }
    public multiply(num: number) {
        if (this.operations.length > 0 && this.operations[this.operations.length - 1].type === OperationType.Multiply) {
            this.operations[this.operations.length - 1].value *= num;
        } else {
            this.operations.push({type: OperationType.Multiply, value: num});
        }
    }
    public add(num: number) {
        if (this.operations.length > 0 && this.operations[this.operations.length - 1].type === OperationType.Add) {
            this.operations[this.operations.length - 1].value += num;
        } else {
            this.operations.push({type: OperationType.Add, value: num});
        }
    }
    public square() {
        this.operations.push({type: OperationType.Square, value: 0});
    }
}

class Monkey {
    public ifTrueMonkey?: Monkey;
    public ifFalseMonkey?: Monkey;
    public numInspections = 0;
    items: OperationStack[];
    constructor(public id:number, itemsNum: number[] = [], public operation: (stack: OperationStack) => void, public testDivisible: number) {
        this.items = itemsNum.map((item) => new OperationStack(item));
    }
    public inspectItem() {
        this.numInspections++;
        const item = this.items.shift()!;
        this.operation(item);
        if (item.getRemainder(this.testDivisible) === 0) {
            // console.log('Item with worry', newWorry, 'is divisible by', this.testDivisible, 'and will be thrown to monkey', this.ifTrueMonkey?.id);
            this.ifTrueMonkey!.items.push(item);
        } else {
            // console.log('Item with worry', newWorry, 'is NOT divisible by', this.testDivisible, 'and will be thrown to monkey', this.ifFalseMonkey?.id);
            this.ifFalseMonkey!.items.push(item);
        }
    }
}

const sample_monkeys = [
    new Monkey(0, [79, 98], (stack) => stack.multiply(19), 23),
    // Monkey 0:
    //   Starting items: 79, 98
    // ], (old) => old * 19
    // , 23
    //     If true: throw to monkey 2
    //     If false: throw to monkey 3
    
    new Monkey(1, [54, 65, 75, 74], (stack) => stack.add(6), 19),
    // Monkey 1:
    //   Starting items: 54, 65, 75, 74
    // ], (old) => old + 6
    // , 19
    //     If true: throw to monkey 2
    //     If false: throw to monkey 0
    
    new Monkey(2, [79, 60, 97], (stack) => stack.square(), 13),
    // Monkey 2:
    //   Starting items: 79, 60, 97
    // ], (old) => old * old
    // , 13
    //     If true: throw to monkey 1
    //     If false: throw to monkey 3
    
    new Monkey(3, [74], (stack) => stack.add(3), 17),
    // Monkey 3:
    //   Starting items: 74
    // ], (old) => old + 3
    // , 17
    //     If true: throw to monkey 0
    //     If false: throw to monkey 1
];
sample_monkeys[0].ifTrueMonkey = sample_monkeys[2];
sample_monkeys[0].ifFalseMonkey = sample_monkeys[3];

sample_monkeys[1].ifTrueMonkey = sample_monkeys[2];
sample_monkeys[1].ifFalseMonkey = sample_monkeys[0];

sample_monkeys[2].ifTrueMonkey = sample_monkeys[1];
sample_monkeys[2].ifFalseMonkey = sample_monkeys[3];

sample_monkeys[3].ifTrueMonkey = sample_monkeys[0];
sample_monkeys[3].ifFalseMonkey = sample_monkeys[1];

const input_monkeys = [
    new Monkey(0,[64], (stack) => stack.multiply(7), 13),
    new Monkey(1,[60, 84, 84, 65], (stack) => stack.add(7), 19),
    new Monkey(2,[52, 67, 74, 88, 51, 61], (stack) => stack.multiply(3), 5),
    new Monkey(3,[67, 72], (stack) => stack.add(3), 2),
    new Monkey(4,[80, 79, 58, 77, 68, 74, 98, 64], (stack) => stack.square(), 17),
    new Monkey(5,[62, 53, 61, 89, 86], (stack) => stack.add(8), 11),
    new Monkey(6,[86, 89, 82], (stack) => stack.add(2), 7),
    new Monkey(7,[92, 81, 70, 96, 69, 84, 83], (stack) => stack.add(4), 3)
];


input_monkeys[0].ifTrueMonkey = input_monkeys[1];
input_monkeys[0].ifFalseMonkey = input_monkeys[3];
input_monkeys[1].ifTrueMonkey = input_monkeys[2];
input_monkeys[1].ifFalseMonkey = input_monkeys[7];
input_monkeys[2].ifTrueMonkey = input_monkeys[5];
input_monkeys[2].ifFalseMonkey = input_monkeys[7];
input_monkeys[3].ifTrueMonkey = input_monkeys[1];
input_monkeys[3].ifFalseMonkey = input_monkeys[2];
input_monkeys[4].ifTrueMonkey = input_monkeys[6];
input_monkeys[4].ifFalseMonkey = input_monkeys[0];
input_monkeys[5].ifTrueMonkey = input_monkeys[4];
input_monkeys[5].ifFalseMonkey = input_monkeys[6];
input_monkeys[6].ifTrueMonkey = input_monkeys[3];
input_monkeys[6].ifFalseMonkey = input_monkeys[0];
input_monkeys[7].ifTrueMonkey = input_monkeys[4];
input_monkeys[7].ifFalseMonkey = input_monkeys[5];

function doRound(monkeys: Monkey[], round: number) {
    // console.log('Starting round', round);
    monkeys.forEach((monkey, i) => {
        // console.log('Processing monkey', i);
        while (monkey.items.length > 0) {
            monkey.inspectItem();
        }
    });
    if (round % 1000 === 0) {
        console.log('At the end of round', round, 'the monkeys have inspected:')
        console.log(monkeys.map((m, i) => `Monkey ${i}: ${m.numInspections}`).join('\n'));
    }
}

function calculateMonkeyBusiness(monkeys: Monkey[], numRounds = 20) {
    for (let round = 1; round <= numRounds; round++) {
        doRound(monkeys, round);
        // if (round % 1000 === 0) {
        //     console.log('After round', round, 'the monkeys have inspected:')
        // }
    }
    const sorted = monkeys.map(m => m.numInspections).sort((a,b) => b-a);
    return sorted[0] * sorted[1];
}
// console.log('Sample answer:', calculateMonkeyBusiness(sample_monkeys));

// console.log('Part 1, What is the level of monkey business after 20 rounds of stuff-slinging simian shenanigans?', calculateMonkeyBusiness(input_monkeys));

/// Part 2
// console.log('Part 2: Sample answer:', calculateMonkeyBusiness(sample_monkeys, 10000));
console.log('Part 2: Answer:', calculateMonkeyBusiness(input_monkeys, 10000));
