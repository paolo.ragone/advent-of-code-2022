// https://adventofcode.com/2022/day/11

import { assert } from "console";
import { loadInputFile_StrByLine, Point } from "../utils";

class Me {
    public worry = 1;
}

interface Reducible {
    getRemainder(base: number): number;
    print(): string;
}
class SimpleNumber implements Reducible {
    constructor(public num: number) {}
    getRemainder(base: number): number {
        return this.num % base;
    }
    print(): string {
        return this.num.toString();
    }
}
class MultiplyBy implements Reducible {
    constructor(public what: Reducible, public by: number) {}
    getRemainder(base: number): number {
        return this.by % base === 0 ? 0 : (this.by % base) * this.what.getRemainder(base);
    }
    print(): string {
        return '{' + this.by.toString() + ' * ' + this.what.print() + '}';
    }
}
class Square implements Reducible {
    constructor(public what: Reducible) {}
    getRemainder(base: number): number {
        const a = this.what.getRemainder(base);
        return (a * a) % base;
    }
    print(): string {
        return '[' + this.what.print() + ' * ' + this.what.print() + ']';
    }
}
class Add implements Reducible {
    constructor(public what: Reducible, public by: number) {}
    getRemainder(base: number): number {
        return (this.what.getRemainder(base) + this.by) % base;
    }
    print(): string {
        return '(' + this.what.print() + ' + ' + this.by.toString() + ')';
    }
}

function multiply(by: number): (old: Reducible) => Reducible {
    return (old: Reducible) => {
        if (old instanceof MultiplyBy) {
            old.by *= by;
            return old;
        } else {
            return new MultiplyBy(old, by);
        }
    }
}

function add(by: number): (old: Reducible) => Reducible {
    return (old: Reducible) => {
        if (old instanceof Add) {
            old.by += by;
            return old;
        } else {
            return new Add(old, by);
        }
    }
}

console.log(multiply(7)(multiply(14)(new SimpleNumber(15))).print());
console.log(add(7)(add(14)(new SimpleNumber(15))).print());
console.log(add(7)(multiply(14)(add(10)(add(4)(new SimpleNumber(15))))).print());



class Monkey {
    public ifTrueMonkey?: Monkey;
    public ifFalseMonkey?: Monkey;
    public numInspections = 0;
    items: Reducible[];
    constructor(public id:number, itemsNum: number[] = [], public operation: (old: Reducible) => Reducible, public testDivisible: number) {
        this.items = itemsNum.map((item) => new SimpleNumber(item));
    }
    public inspectItem() {
        this.numInspections++;
        const item = this.items.shift()!;
        const afterThrow = this.operation(item);
        if (afterThrow.getRemainder(this.testDivisible) === 0) {
            // console.log('Item with worry', newWorry, 'is divisible by', this.testDivisible, 'and will be thrown to monkey', this.ifTrueMonkey?.id);
            this.ifTrueMonkey!.items.push(afterThrow);
        } else {
            // console.log('Item with worry', newWorry, 'is NOT divisible by', this.testDivisible, 'and will be thrown to monkey', this.ifFalseMonkey?.id);
            this.ifFalseMonkey!.items.push(afterThrow);
        }
    }
}

const sample_monkeys = [
    new Monkey(0, [79, 98], (old) => new MultiplyBy(old, 19), 23),
    // Monkey 0:
    //   Starting items: 79, 98
    // ], (old) => old * 19
    // , 23
    //     If true: throw to monkey 2
    //     If false: throw to monkey 3
    
    new Monkey(1, [54, 65, 75, 74], (old) => new Add(old, 6), 19),
    // Monkey 1:
    //   Starting items: 54, 65, 75, 74
    // ], (old) => old + 6
    // , 19
    //     If true: throw to monkey 2
    //     If false: throw to monkey 0
    
    new Monkey(2, [79, 60, 97], (old) => new Square(old), 13),
    // Monkey 2:
    //   Starting items: 79, 60, 97
    // ], (old) => old * old
    // , 13
    //     If true: throw to monkey 1
    //     If false: throw to monkey 3
    
    new Monkey(3, [74], (old) => new Add(old, 3), 17),
    // Monkey 3:
    //   Starting items: 74
    // ], (old) => old + 3
    // , 17
    //     If true: throw to monkey 0
    //     If false: throw to monkey 1
];
sample_monkeys[0].ifTrueMonkey = sample_monkeys[2];
sample_monkeys[0].ifFalseMonkey = sample_monkeys[3];

sample_monkeys[1].ifTrueMonkey = sample_monkeys[2];
sample_monkeys[1].ifFalseMonkey = sample_monkeys[0];

sample_monkeys[2].ifTrueMonkey = sample_monkeys[1];
sample_monkeys[2].ifFalseMonkey = sample_monkeys[3];

sample_monkeys[3].ifTrueMonkey = sample_monkeys[0];
sample_monkeys[3].ifFalseMonkey = sample_monkeys[1];

const input_monkeys = [
    new Monkey(0,[64], multiply(7), 13),
    new Monkey(1,[60, 84, 84, 65], add(7), 19),
    new Monkey(2,[52, 67, 74, 88, 51, 61], multiply(3), 5),
    new Monkey(3,[67, 72], add(3), 2),
    new Monkey(4,[80, 79, 58, 77, 68, 74, 98, 64], (old) => new Square(old), 17),
    new Monkey(5,[62, 53, 61, 89, 86], add(8), 11),
    new Monkey(6,[86, 89, 82], add(2), 7),
    new Monkey(7,[92, 81, 70, 96, 69, 84, 83], add(4), 3)
];


input_monkeys[0].ifTrueMonkey = input_monkeys[1];
input_monkeys[0].ifFalseMonkey = input_monkeys[3];
input_monkeys[1].ifTrueMonkey = input_monkeys[2];
input_monkeys[1].ifFalseMonkey = input_monkeys[7];
input_monkeys[2].ifTrueMonkey = input_monkeys[5];
input_monkeys[2].ifFalseMonkey = input_monkeys[7];
input_monkeys[3].ifTrueMonkey = input_monkeys[1];
input_monkeys[3].ifFalseMonkey = input_monkeys[2];
input_monkeys[4].ifTrueMonkey = input_monkeys[6];
input_monkeys[4].ifFalseMonkey = input_monkeys[0];
input_monkeys[5].ifTrueMonkey = input_monkeys[4];
input_monkeys[5].ifFalseMonkey = input_monkeys[6];
input_monkeys[6].ifTrueMonkey = input_monkeys[3];
input_monkeys[6].ifFalseMonkey = input_monkeys[0];
input_monkeys[7].ifTrueMonkey = input_monkeys[4];
input_monkeys[7].ifFalseMonkey = input_monkeys[5];

function doRound(monkeys: Monkey[], round: number) {
    // console.log('Starting round', round);
    monkeys.forEach((monkey, i) => {
        // console.log('Processing monkey', i);
        while (monkey.items.length > 0) {
            monkey.inspectItem();
        }
    });
    if (round % 1000 === 0) {
        console.log('At the end of round', round, 'the monkeys have inspected:')
        console.log(monkeys.map((m, i) => `Monkey ${i}: ${m.numInspections}`).join('\n'));
    }
}

function calculateMonkeyBusiness(monkeys: Monkey[], numRounds = 20) {
    for (let round = 1; round <= numRounds; round++) {
        doRound(monkeys, round);
        // if (round % 1000 === 0) {
        //     console.log('After round', round, 'the monkeys have inspected:')
        // }
    }
    const sorted = monkeys.map(m => m.numInspections).sort((a,b) => b-a);
    return sorted[0] * sorted[1];
}
// console.log('Sample answer:', calculateMonkeyBusiness(sample_monkeys));

// console.log('Part 1, What is the level of monkey business after 20 rounds of stuff-slinging simian shenanigans?', calculateMonkeyBusiness(input_monkeys));

/// Part 2
// console.log('Part 2: Sample answer:', calculateMonkeyBusiness(sample_monkeys, 10000));
