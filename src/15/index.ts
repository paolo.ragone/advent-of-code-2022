// https://adventofcode.com/2022/day/15

import { loadInputFile_StrByLine, manhattanDistance, Point, Range } from "../utils";

class Sensor {
    distanceToBeacon: number;
    constructor(public x: number, public y: number, public closestBeacon: Beacon) {
        this.distanceToBeacon = manhattanDistance({ x, y }, { x: closestBeacon.x, y: closestBeacon.y });
    }
    wouldDetectBeaconAt(x: number, y: number): boolean {
        return manhattanDistance({ x, y }, { x: this.x, y: this.y }) <= this.distanceToBeacon;
    }
    isSensorCoveringRow(y: number): boolean {
        return Math.abs(this.y - y) <= this.distanceToBeacon;
    }
    getRangeCoveredInRow(y: number): Range {
        const delta = this.distanceToBeacon - Math.abs(this.y - y);
        return new Range(this.x - delta, this.x + delta);
    }
}
class Beacon {
    constructor(public x: number, public y: number) { }
}

function cleanupOverlappingRanges(ranges: Range[]): Range[] {
    const sorted = ranges.sort((a, b) => a.from - b.from);
    const result: Range[] = [];
    for (const range of sorted) {
        if (result.length === 0) {
            result.push(range);
        } else {
            const last = result[result.length - 1];
            if (last.intersectsWith(range) || last.neighbours(range)) {
                result[result.length - 1] = last.unionWith(range);
            } else {
                result.push(range);
            }
        }
    }
    return result;
}

class Grid {
    public sensors: Sensor[] = [];
    public beacons: Beacon[] = [];

    getOrCreateBeaconAt(x: number, y: number): Beacon {
        const existing = this.beacons.find(b => b.x === x && b.y === y);
        if (existing) return existing;
        const newBeacon = new Beacon(x, y);
        this.beacons.push(newBeacon);
        return newBeacon;
    }
    addSensor(sx: number, sy: number, beacon: Beacon) {
        this.sensors.push(new Sensor(sx, sy, beacon));
    }
    getSensorsCoveringRow(y: number): Sensor[] {
        return this.sensors.filter(s => s.isSensorCoveringRow(y));
    }
    getNumPositionsCoveredBySensorsInRow(y: number): number {
        const sensors = this.getSensorsCoveringRow(y);
        const ranges = sensors.map(s => s.getRangeCoveredInRow(y));
        const reducedRanges = cleanupOverlappingRanges(ranges);
        return reducedRanges.reduce((sum, r) => sum + r.length, 0) - this.beacons.filter(b => b.y === y).length;
    }
    getDistressSignalPosition(min: number, max: number): Point | undefined {
        const search_range = new Range(min, max);
        for (let y = min; y <= max; y++) {
            const sensors = this.getSensorsCoveringRow(y);
            const ranges = sensors.map(s => s.getRangeCoveredInRow(y));
            const reducedRanges = cleanupOverlappingRanges(ranges);
            if (!reducedRanges.some(r => r.contains(search_range))) {
                const start_range = new Range(min, max);
                const remainder = reducedRanges
                    .filter(r => r.intersectsWith(start_range))
                    .reduce((remainder, to_remove) => {
                        if (remainder.from <= to_remove.from) {
                            return new Range(remainder.from, to_remove.from - 1);
                        } else {
                            return new Range(to_remove.to + 1, remainder.to);
                        }
                    }, start_range);
                if (remainder.length === 1) {
                    return { x: remainder.from, y };
                }
            }
        }
    }
}

// The format of a line is: "Sensor at x=2, y=18: closest beacon is at x=-2, y=15"
function parseLine(line: string, grid: Grid) {
    const [sx, sy, bx, by] = line.match(/Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)/)!.slice(1).map(s => parseInt(s, 10));
    const beacon = grid.getOrCreateBeaconAt(bx, by);
    grid.addSensor(sx, sy, beacon);

}
 

const sample_input = loadInputFile_StrByLine(__dirname, 'sample_input.txt');
const sample_grid = new Grid();
sample_input.forEach(line => parseLine(line, sample_grid));

const sample_sensors_covering_row = sample_grid.getSensorsCoveringRow(10);
console.assert(sample_sensors_covering_row.length === 6, 'Wrong number of covering sensors. Expected 6, got ' + sample_sensors_covering_row.length);
const sample_num_positions_covered = sample_grid.getNumPositionsCoveredBySensorsInRow(10);
console.assert(sample_num_positions_covered === 26, 'Wrong number of covered positions. Expected 26, got ' + sample_num_positions_covered);

const sample_distress_signal = sample_grid.getDistressSignalPosition(0, 20);
console.assert(sample_distress_signal!.x === 14 && sample_distress_signal!.y === 11, 'Wrong distress signal position. Expected {x: 14, y: 11}, got ' + JSON.stringify(sample_distress_signal));

const input = loadInputFile_StrByLine(__dirname, 'input.txt');
const grid = new Grid();
input.forEach(line => parseLine(line, grid));

console.log(`Part 1: In the row where y=2000000, how many positions cannot contain a beacon? ${grid.getNumPositionsCoveredBySensorsInRow(2000000)}`)
const distress_signal = grid.getDistressSignalPosition(0, 4000000);
console.log(`Part 2: What is its tuning frequency? ${distress_signal!.x * 4000000 + distress_signal!.y}`)