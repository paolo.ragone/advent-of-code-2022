// https://adventofcode.com/2022/day/10

import { assert } from "console";
import { loadInputFile_StrByLine, Point } from "../utils";

class Processor {
    public cycle: number = 1;
    public value: number = 1;
    public instructions: string[] = [];
    public crt: string[][] = [0,1,2,3,4,5].map(() => new Array(40).fill('.', 0, 40));

    public addInstruction(input: string) {
        this.instructions.push(input);
    }

    // input can be one of: noop or addx <number>
    public tick() {
        this.cycle++;
        const input = this.instructions.shift()
        if (!input) {
            return;
        }
        if (input === 'noop') {
            return; // Nothing else to do
        }
        if (input.startsWith('2addx')) {
            const [_, value] = input.split(' ');
            this.value += parseInt(value, 10);
        } else if (input.startsWith('addx')) {
            this.instructions.unshift('2' + input);
            // Skip a cycle.
        }
    }
    public paint() {
        const pos = this.cycle -1;
        const x = pos % 40;
        const y = Math.floor(pos / 40);
        console.log(`On cycle ${this.cycle} - Pos (${x}, ${y}) - value = ${this.value}`)
        if (Math.abs(x  - this.value) <= 1) {
            this.crt[y][x] = '#';
        }
    }
    public processAll(): number {
        let acum = 0;
        while (this.instructions.length > 0) {
            this.paint();
            this.tick();
            if (this.cycle % 40 === 20) {
                console.log(`Cycle ${this.cycle} - Value ${this.value} => ${this.cycle * this.value}`)
                acum += this.cycle * this.value;
            }
        }
        return acum;
    }
    public getCRT(): string {
        return this.crt.map((line) => line.join('')).join('\n');
    }
}


// const sample_input = loadInputFile_StrByLine(__dirname, 'sample_input.txt');
// const sample_processor = new Processor();
// sample_input.forEach((line) => {
//     sample_processor.addInstruction(line);
// });
// console.assert(sample_processor.processAll() === 13140, 'Wrong result on sample');
// console.log(sample_processor.getCRT());

const input = loadInputFile_StrByLine(__dirname, 'input.txt');
const processor = new Processor();
input.forEach((line) => {
    processor.addInstruction(line);
});
console.log(`Part 1: What is the sum of these six signal strengths? ${processor.processAll()}`);
console.log(processor.getCRT());

