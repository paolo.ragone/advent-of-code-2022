// https://adventofcode.com/2022/day/3

import { loadInputFile_StrByLine } from "../utils";

const raw_input = loadInputFile_StrByLine(__dirname, 'input.txt');
const rucksacks = raw_input.map(l => [l.substring(0, l.length/2), l.substring(l.length/2)]);

const code_a = 'a'.charCodeAt(0);
const code_A = 'A'.charCodeAt(0);

function getTypePriority(typeStr: string): number {
  const code = typeStr.charCodeAt(0);
  return (code > code_a) ? (code - code_a + 1) : (code - code_A + 27);
}

function getTypes(content: string): Set<string> {
  return new Set<string>([...content]);
}

const acum = rucksacks.map(r => {
  const left = r[0];
  const right = r[1];
  const leftTypes = getTypes(left);
  const rightTypes = getTypes(right);
  const shared = Array.from(leftTypes).filter(l => rightTypes.has(l));
  // console.log(`For rucksak with types [[${Array.from(leftTypes)}], [${Array.from(rightTypes)}]], shared is: ${Array.from(shared)}, value is ${getTypePriority(shared[0])}`);
  return getTypePriority(shared[0]);
}).reduce((a,v) => a+v, 0);

console.log('Part 1: What is the sum of the priorities of those item types?', acum);

const chunkSize = 3;
let acum2 = 0;
for (let i = 0; i < raw_input.length; i += chunkSize) {
  const chunk = raw_input.slice(i, i + chunkSize);
  const chunkTypes = chunk.map(getTypes);
  const shared = Array.from(chunkTypes[0]).filter(l => chunkTypes[1].has(l) && chunkTypes[2].has(l));
  acum2 += getTypePriority(shared[0]);
}

console.log('Part 2: What is the sum of the priorities of those item types?', acum2);
