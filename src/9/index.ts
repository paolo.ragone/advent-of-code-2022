// https://adventofcode.com/2022/day/9

import { assert } from "console";
import { loadInputFile_StrByLine, Point } from "../utils";

class Knot {
    public position: Point;
    public following?: Knot = undefined;
    public visitedPositions: Point[] = [];

    constructor(startPosition: Point = {x: 0, y: 0}) {
        this.position = startPosition;
        this.visitedPositions.push({...this.position});
    }
    public processInput(input: string) {
        const [direction, steps] = input.split(' ');
        this.move(direction, parseInt(steps, 10));
    }
    public move(direction: string, steps: number) {
        for (let i = 0; i < steps; i++) {
            this.moveOne(direction);
        }
    }
    public moveOne(direction: string) {
        switch (direction) {
            case 'R':
                this.position.x += 1;
                break;
            case 'U':
                this.position.y += 1;
                break;
            case 'L':
                this.position.x -= 1;
                break;
            case 'D':
                this.position.y -= 1;
                break;
        }
        this.visitedPositions.push({...this.position});
        if (this.following) {
            this.following.followPosition(this.position);
        }
    }
    public followPosition(head: Point) {
        const startPosition = {...this.position};
        if (Math.abs(head.x - this.position.x) > 1 && head.y === this.position.y) {
            this.position.x += Math.max(-1, Math.min(1, head.x - this.position.x));
        } else if (Math.abs(head.y - this.position.y) > 1 && head.x === this.position.x) {
            this.position.y += Math.max(-1, Math.min(1, head.y - this.position.y));
        } else if (Math.abs(head.x - this.position.x) > 1 || Math.abs(head.y - this.position.y) > 1) {
            this.position.x += Math.max(-1, Math.min(1, head.x - this.position.x));
            this.position.y += Math.max(-1, Math.min(1, head.y - this.position.y));
        }
        this.visitedPositions.push({...this.position});
        if (this.following && (this.position.x != startPosition.x || this.position.y != startPosition.y)) {
            this.following.followPosition(this.position);
        }
    }
    public getNumDistinctVisitedPositions(): number {
        const visitedPositionsSet = new Set(this.visitedPositions.map((point) => `${point.x},${point.y}`));
        return visitedPositionsSet.size;
    }
}

// class Grid {
//     public head: Point = {x: 0, y: 0};
//     public tail: Point = {x: 0, y: 0};
//     public visitedTailPoints: Point[] = [];

//     // directions are R, U, L, D
//     public moveHead(direction: string, steps: number) {
//         for (let i = 0; i < steps; i++) {
//             this.moveHeadOne(direction);
//         }
//     }
//     // input is in format "R 3"
//     public processInput(input: string) {
//         const [direction, steps] = input.split(' ');
//         this.moveHead(direction, parseInt(steps, 10));
//     }
//     public checkTail() {
//         if (Math.abs(this.head.x - this.tail.x) > 1 && this.head.y === this.tail.y) {
//             this.tail.x += Math.max(-1, Math.min(1, this.head.x - this.tail.x));
//         } else if (Math.abs(this.head.y - this.tail.y) > 1 && this.head.x === this.tail.x) {
//             this.tail.y += Math.max(-1, Math.min(1, this.head.y - this.tail.y));
//         } else if (Math.abs(this.head.x - this.tail.x) > 1 || Math.abs(this.head.y - this.tail.y) > 1) {
//             this.tail.x += Math.max(-1, Math.min(1, this.head.x - this.tail.x));
//             this.tail.y += Math.max(-1, Math.min(1, this.head.y - this.tail.y));
//         }
//         this.visitedTailPoints.push({...this.tail});
//     }
//     public moveHeadOne(direction: string) {
//         switch (direction) {
//             case 'R':
//                 this.head.x += 1;
//                 break;
//             case 'U':
//                 this.head.y += 1;
//                 break;
//             case 'L':
//                 this.head.x -= 1;
//                 break;
//             case 'D':
//                 this.head.y -= 1;
//                 break;
//         }
//         this.checkTail();
//         // console.log('Moving ', direction, ' head is now at', this.head, 'and tail is now at', this.tail)
//     }
//     public getNumDistinctVisitedTailPoints(): number {
//         const visitedTailPointsSet = new Set(this.visitedTailPoints.map((point) => `${point.x},${point.y}`));
//         return visitedTailPointsSet.size;
//     }
// }

// const testGrid = new Grid();
// testGrid.moveHead('R', 4);
// assert(testGrid.head.x === 4 && testGrid.head.y === 0);
// assert(testGrid.tail.x === 3 && testGrid.tail.y === 0);
// testGrid.moveHead('U', 4);
// assert(testGrid.head.x === 4 && testGrid.head.y === 4);
// assert(testGrid.tail.x === 4 && testGrid.tail.y === 3);
// testGrid.moveHead('L', 3);
// assert(testGrid.head.x === 1 && testGrid.head.y === 4, 'head is at ' + JSON.stringify(testGrid.head));
// assert(testGrid.tail.x === 2 && testGrid.tail.y === 4, 'head is at ' + JSON.stringify(testGrid.head));
// testGrid.moveHead('D', 1);
// assert(testGrid.head.x === 1 && testGrid.head.y === 3);
// assert(testGrid.tail.x === 2 && testGrid.tail.y === 4);
// testGrid.moveHead('R', 4);
// assert(testGrid.head.x === 5 && testGrid.head.y === 3);
// assert(testGrid.tail.x === 4 && testGrid.tail.y === 3);
// testGrid.moveHead('D', 1);
// assert(testGrid.head.x === 5 && testGrid.head.y === 2);
// assert(testGrid.tail.x === 4 && testGrid.tail.y === 3);
// testGrid.moveHead('L', 5);
// assert(testGrid.head.x === 0 && testGrid.head.y === 2);
// assert(testGrid.tail.x === 1 && testGrid.tail.y === 2);
// testGrid.moveHead('R', 2);
// assert(testGrid.head.x === 2 && testGrid.head.y === 2);
// assert(testGrid.tail.x === 1 && testGrid.tail.y === 2);

function runPart1Sample() {
    const sampleKnots = [1,2,3,4,5,6,7,8,9,10].map((i) => new Knot());
    for (let i = 1; i < sampleKnots.length; i++) {
        sampleKnots[i-1].following = sampleKnots[i];
    }
    const sampleHead = sampleKnots[0];
    const sampleTail = sampleKnots[1];
    const third = sampleKnots[3];
    sampleHead.move('R', 4);
    assert(sampleHead.position.x === 4 && sampleHead.position.y === 0);
    assert(sampleTail.position.x === 3 && sampleTail.position.y === 0);
    assert(third.position.x === 1 && third.position.y === 0);
    sampleHead.move('U', 4);
    assert(sampleHead.position.x === 4 && sampleHead.position.y === 4);
    assert(sampleTail.position.x === 4 && sampleTail.position.y === 3);
    assert(third.position.x === 3 && third.position.y === 2);
    sampleHead.move('L', 3);
    assert(sampleHead.position.x === 1 && sampleHead.position.y === 4);
    assert(sampleTail.position.x === 2 && sampleTail.position.y === 4);
    assert(third.position.x === 3 && third.position.y === 2);
    sampleHead.move('D', 1);
    assert(sampleHead.position.x === 1 && sampleHead.position.y === 3);
    assert(sampleTail.position.x === 2 && sampleTail.position.y === 4);
    assert(third.position.x === 3 && third.position.y === 2);
    sampleHead.move('R', 4);
    assert(sampleHead.position.x === 5 && sampleHead.position.y === 3);
    assert(sampleTail.position.x === 4 && sampleTail.position.y === 3);
    assert(third.position.x === 3 && third.position.y === 2);
    sampleHead.move('D', 1);
    assert(sampleHead.position.x === 5 && sampleHead.position.y === 2);
    assert(sampleTail.position.x === 4 && sampleTail.position.y === 3);
    assert(third.position.x === 3 && third.position.y === 2);
    sampleHead.move('L', 5);
    assert(sampleHead.position.x === 0 && sampleHead.position.y === 2);
    assert(sampleTail.position.x === 1 && sampleTail.position.y === 2);
    assert(third.position.x === 3 && third.position.y === 2);
    sampleHead.move('R', 2);
    assert(sampleHead.position.x === 2 && sampleHead.position.y === 2);
    assert(sampleTail.position.x === 1 && sampleTail.position.y === 2);
    assert(third.position.x === 3 && third.position.y === 2);

    assert(sampleTail.getNumDistinctVisitedPositions() === 13, 'Wrong number of visited points');
}

runPart1Sample();

// Part 1


const knots = [1,2,3,4,5,6,7,8,9,10].map((i) => new Knot());
for (let i = 1; i < knots.length; i++) {
    knots[i-1].following = knots[i];
}
const input = loadInputFile_StrByLine(__dirname, 'input.txt');
input.forEach((i) => {
    knots[0].processInput(i);
});
console.log('Part 1: How many positions does the tail of the rope visit at least once?', knots[1].getNumDistinctVisitedPositions());


// Part 2

function runPart2Sample() {
    
    const sampleKnots = [1,2,3,4,5,6,7,8,9,10].map((i) => new Knot());
    for (let i = 1; i < sampleKnots.length; i++) {
        sampleKnots[i-1].following = sampleKnots[i];
    }
    const sampleHead = sampleKnots[0];
    const sampleTail = sampleKnots[1];
    const third = sampleKnots[3];
    sampleHead.move('R', 5);
    assert(sampleHead.position.x === 5 && sampleHead.position.y === 0, 'Failed 1');
    sampleHead.move('U', 8);
    assert(sampleHead.position.x === 5 && sampleHead.position.y === 8, 'Failed 2');
    sampleHead.move('L', 8);
    assert(sampleHead.position.x === -3 && sampleHead.position.y === 8, 'Failed 3');
    sampleHead.move('D', 3);
    assert(sampleHead.position.x === -3 && sampleHead.position.y === 5, 'Failed 4');
    sampleHead.move('R', 17);
    assert(sampleHead.position.x === 14 && sampleHead.position.y === 5, 'Failed 5');
    sampleHead.move('D', 10);
    assert(sampleHead.position.x === 14 && sampleHead.position.y === -5, 'Failed 6');
    sampleHead.move('L', 25);
    assert(sampleHead.position.x === -11 && sampleHead.position.y === -5, 'Failed 7');
    sampleHead.move('U', 20);
    assert(sampleHead.position.x === -11 && sampleHead.position.y === 15, 'Failed 8');
    
    assert(sampleKnots[9].getNumDistinctVisitedPositions() === 36, 'Wrong number of visited points. Part 2');
}

runPart2Sample();

console.log('Part 2: How many positions does the tail of the rope visit at least once?', knots[9].getNumDistinctVisitedPositions());
