// https://adventofcode.com/2022/day/7

import { assert } from "console";
import { loadInputFile_StrByLine } from "../utils";


const total_filesystem_size = 70000000;
const needed_space = 30000000;

class File {
    constructor(public name: string, protected size: number = 0) {}
    getSize() {
        return this.size;
    }
}

class Folder extends File {
    private files: File[] = [];
    constructor(public name: string, public parent: Folder | null = null) {
        super(name, 0);
    }
    addFile(name: string, size: number) {
        this.files.push(new File(name, size));
    }
    getSize(): number {
        return this.files.reduce((acc, f) => acc + f.getSize(), 0);
    }
    addFolder(name: string): Folder {
        const existing = this.files.filter((f) => f.name === name);
        if (existing.length > 0) {
            return existing[0] as Folder;
        }
        const folder = new Folder(name, this);
        this.files.push(folder);
        return  folder;
    }
    getAllFolders(): Folder[] {
        return [this, ...(this.files.filter((f) => f instanceof Folder) as Folder[]).flatMap((f) => f.getAllFolders())];
    }
}

class OutputProcessor {
    public root = new Folder('ROOT');
    private pwd = this.root;
    private buffer: string[] = [];
    constructor() {}
    processCommandOutput(lines: string[]) {
        const commandLine = lines[0];
        const [_, command, path] = commandLine.split(' ');
        if (command === 'cd') {
            if (path === '/') {
                this.pwd = this.root;
            } else if (path === '..') {
                this.pwd = this.pwd.parent || this.root;
            } else {
                this.pwd = this.pwd.addFolder(path);
            }
        } else if (command === 'ls') {
            lines.slice(1).forEach((line) => {
                const [size, name] = line.split(' ');
                if (size === 'dir') {
                    this.pwd.addFolder(name);
                } else {
                    this.pwd.addFile(name, parseInt(size));
                }
            });
        }
    }
    processLine(line: string) {
        if (line.startsWith('$')) {
            if (this.buffer.length > 0) {
                this.processCommandOutput(this.buffer);
                this.buffer = [];
            }
        }
        this.buffer.push(line);
    }
    end() {
        if (this.buffer.length > 0) {
            this.processCommandOutput(this.buffer);
            this.buffer = [];
        }
    }
}

const raw_sample_input = loadInputFile_StrByLine(__dirname, 'sample_input.txt');
const sample_processor = new OutputProcessor();
raw_sample_input.forEach((line) => sample_processor.processLine(line));
sample_processor.end();

assert(sample_processor.root.getSize() === 48381165);
// console.log(sample_processor.root.getAllFolders())
assert(sample_processor.root.getAllFolders().length === 4);

const sample_size_of_small_dirs = sample_processor.root.getAllFolders().filter(f => f.getSize() < 100000).reduce((acum, f) => acum + f.getSize(), 0)
assert(sample_size_of_small_dirs === 95437);

/////

const sample_free_space = total_filesystem_size - sample_processor.root.getSize();
const sample_need_to_free = needed_space - sample_free_space;

const sample_folder_sizes = sample_processor.root.getAllFolders().map(f => ({folder:f, size:f.getSize()}));
sample_folder_sizes.sort((a, b) => a.size - b.size);
const sample_folder_to_remove = sample_folder_sizes.find(f => f.size >=sample_need_to_free);
assert(sample_folder_to_remove?.size === 24933642);



const raw_input = loadInputFile_StrByLine(__dirname, 'input.txt');
const processor = new OutputProcessor();
raw_input.forEach((line) => processor.processLine(line));
processor.end();
const size_of_small_dirs = processor.root.getAllFolders().filter(f => f.getSize() < 100000).reduce((acum, f) => acum + f.getSize(), 0)
console.log('Part 1: What is the sum of the total sizes of those directories?', size_of_small_dirs);

const free_space = total_filesystem_size - processor.root.getSize();
const need_to_free = needed_space - free_space;

const folder_sizes = processor.root.getAllFolders().map(f => ({folder:f, size:f.getSize()}));
folder_sizes.sort((a, b) => a.size - b.size);
const folder_to_remove = folder_sizes.find(f => f.size >= need_to_free);

console.log('Part 2: What is the total size of that directory?', folder_to_remove?.size);