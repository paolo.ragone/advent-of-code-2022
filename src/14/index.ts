// https://adventofcode.com/2022/day/14

import { loadInputFile_StrByLine, Point } from "../utils";

class SparseGrid {
    public grid: Map<Number, Map<Number, string>> = new Map();

    drawFloor(from: Point) {
        const floorY = this.getMaxY() + 2;
        const delta = Math.ceil((floorY - from.y) * 1.1);
        this.addLine({x: from.x - delta, y: floorY}, {x: from.x + delta, y: floorY});
    }
    getMaxY() {
        let maxY = 0;
        for (const column of this.grid.values()) {
            const columnMaxY = Math.max(...Array.from(column.keys()).map(v => v.valueOf()));
            if (columnMaxY > maxY) {
                maxY = columnMaxY;
            } 
        }
        return maxY;
    }
    addPath(points: Point[]) {
        for (let i = 0; i < points.length - 1; i++) {
            const p1 = points[i];
            const p2 = points[i + 1];
            this.addLine(p1, p2);
        }
    }
    addLine(p1: Point, p2: Point) {
        const {x: x1, y: y1} = p1;
        const {x: x2, y: y2} = p2;
        const xRange = x1 < x2 ? [x1, x2] : [x2, x1];
        const yRange = y1 < y2 ? [y1, y2] : [y2, y1];
        for (let x = xRange[0]; x <= xRange[1]; x++) {
            for (let y = yRange[0]; y <= yRange[1]; y++) {
                if (!this.grid.has(x)) {
                    this.grid.set(x, new Map());
                }
                this.grid.get(x)!.set(y, "#");
            }
        }
    }
    public isFree(point: Point): boolean {
        const {x, y} = point;
        const column = this.grid.get(x);
        if (!column) {
            return true;
        }
        return !column.has(y);
    }
    addSandFrom(point: Point): Point | undefined {
        const {x, y} = point;
        const column = this.grid.get(x);
        if (!column) {
            return undefined; // Falls on the void
        }
        const stopAt = Array.from(column.keys()).map(v => v.valueOf()).sort((a, b) => a - b).filter(y_p => y_p > y)[0]-1;
        if (this.isFree({x: x - 1, y: stopAt+1})) {
            return this.addSandFrom({x: x - 1, y: stopAt+1});
        }
        if (this.isFree({x: x + 1, y: stopAt+1})) {
            return this.addSandFrom({x: x + 1, y: stopAt+1});
        }
        column.set(stopAt, "o");
        return {x, y: stopAt};
    }
    addSandOk(point: Point): boolean {
        const lastSand = this.addSandFrom(point); 
        return lastSand !== undefined && !(lastSand.x === point.x && lastSand.y === point.y);
    }
    howMuchSandFrom(point: Point): number {
        let numSand = 0;
        while (this.addSandOk(point)) {
            numSand++;
        }
        if (!this.isFree(point)) {
            numSand++;
        }
        return numSand;
    }
}

function parseInput(input: string[]): SparseGrid {
    const grid = new SparseGrid();
    for (const line of input) {
        const path: Point[] = line.split(" -> ").map(p => {
            const parts = p.trim().split(",").map(v => parseInt(v))
            return {x: parts[0], y: parts[1]};
        });
        grid.addPath(path);
    }
    return grid;
}

const startPoint = {x: 500, y: 0};
const sample_input = loadInputFile_StrByLine(__dirname, 'sample_input.txt');
const sample_grid = parseInput(sample_input);
console.assert(sample_grid.isFree({x: 500, y: 9}) === false);
console.assert(sample_grid.isFree({x: 498, y: 5}) === false);
console.assert(sample_grid.isFree({x: 500, y: 8}) === true);
const expectedSandLocations = [
    {x: 500, y: 8},
    {x: 499, y: 8},
    {x: 501, y: 8},
    {x: 500, y: 7},
    {x: 498, y: 8},
];
expectedSandLocations.forEach((expected, i) => {
    const sandAt = sample_grid.addSandFrom(startPoint);
    console.assert(sandAt !== undefined, `Expected sand at ${expected.x},${expected.y} but got undefined at iteration ${i}`);
    console.assert(sandAt!.x === expected.x && sandAt!.y === expected.y, `Expected sand at ${expected.x},${expected.y} but got ${sandAt!.x},${sandAt!.y} at iteration ${i}`);
});

const sample_grid2 = parseInput(sample_input);
const sample_num_sand = sample_grid2.howMuchSandFrom(startPoint);
console.assert(sample_num_sand === 24, `Expected 24 sand but got ${sample_num_sand}`);

const sample_grid3 = parseInput(sample_input);
sample_grid3.drawFloor(startPoint);
const sample_num_sand3 = sample_grid3.howMuchSandFrom(startPoint);
console.assert(sample_num_sand3 === 93, `Expected 93 sand but got ${sample_num_sand3}`);


const input = loadInputFile_StrByLine(__dirname, 'input.txt');
const grid = parseInput(input);
const num_sand = grid.howMuchSandFrom(startPoint);
console.log(`Part 1: How many units of sand come to rest before sand starts flowing into the abyss below? ${num_sand}`);

const grid2 = parseInput(input);
grid2.drawFloor(startPoint);
const num_sand2 = grid2.howMuchSandFrom(startPoint);
console.log(`Part 2: How many units of sand come to rest? ${num_sand2}`);
