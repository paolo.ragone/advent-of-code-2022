// https://adventofcode.com/2022/day/13

import { loadInputFile_StrByLine, splitArrayIntoSubArrays } from "../utils";

type Packet = Number | Array<Packet>

function goesBefore(left: Packet, right: Packet): boolean | undefined {
    if (typeof left === 'number' && typeof right === 'number') {
        if (left === right) return undefined;
        return left < right;
    }
    if (Array.isArray(left) && typeof right === 'number') {
        return goesBefore(left, [right]);
    }
    if (Array.isArray(right) && typeof left === 'number') {
        return goesBefore([left], right);
    }
    // They're both arrays
    if (Array.isArray(left) && Array.isArray(right)) { // <-- This is always true, but the compiler needs a bit of help ;)
        for (let i = 0; i < left.length; i++) {
            if (i >= right.length) {
                return false;
            }
            if (left[i] !== right[i]) {
                const res = goesBefore(left[i], right[i]);
                if (res !== undefined) {
                    return res;
                }
            }
        }
        if (left.length < right.length) {
            return true;
        }
    }
    return undefined;
}

console.assert(goesBefore([1,1,3,1,1], [1,1,5,1,1]), 'Expected true 1');
console.assert(goesBefore([1], [1]) === undefined, 'Expected undefined 2');
console.assert(goesBefore(1, 1) === undefined, 'Expected undefined 3');
console.assert(goesBefore([[1],[2,3,4]], [[1],4]), 'Expected true 4');
console.assert(goesBefore([9], [[8,7,6]]) === false, 'Expected false 5');
console.assert(goesBefore([[4,4],4,4], [[4,4],4,4,4]), 'Expected true 6');
console.assert(goesBefore([7,7,7,7], [7,7,7]) === false, 'Expected false 7');
console.assert(goesBefore([], [3]), 'Expected true 8');
console.assert(goesBefore([[[]]], [[]]) === false, 'Expected false 9');
console.assert(goesBefore([1,[2,[3,[4,[5,6,7]]]],8,9], [1,[2,[3,[4,[5,6,0]]]],8,9]) === false, 'Expected false 10');

const sample_input = loadInputFile_StrByLine(__dirname, 'sample_input.txt', false);
const sample_pairs = splitArrayIntoSubArrays(sample_input, (s) => s === '').map((pair) => pair.map((s) => eval(s)));
const sample_resp = sample_pairs.reduce((acum, pair, index) => goesBefore(pair[0], pair[1]) === true ? acum + index + 1 : acum, 0);
console.assert(sample_resp === 13, 'Wrong expected answer');

const input = loadInputFile_StrByLine(__dirname, 'input.txt', false);
const pairs = splitArrayIntoSubArrays(input, (s) => s === '').map((pair) => pair.map((s) => eval(s)));
const resp = pairs.reduce((acum, pair, index) => goesBefore(pair[0], pair[1]) === true ? acum + index + 1 : acum, 0);
console.log(`Part 1: What is the sum of the indices of those pairs?`, resp);

// Part 2

const sample_input2 = loadInputFile_StrByLine(__dirname, 'sample_input.txt');
const sample_packets = sample_input2.map((s) => eval(s));
sample_packets.push([[2]]);
sample_packets.push([[6]]);
sample_packets.sort((l, r) => { const res = goesBefore(l,r); return res === true ? -1 : res === false ? 1 : 0; });
const sample_packets_str = sample_packets.map((p) => JSON.stringify(p))
const sample_key = (sample_packets_str.indexOf('[[2]]') + 1) * (sample_packets_str.indexOf('[[6]]') + 1)
console.assert(sample_key === 140, 'Wrong expected key');



const input2 = loadInputFile_StrByLine(__dirname, 'input.txt');
const packets = input2.map((s) => eval(s));
packets.push([[2]]);
packets.push([[6]]);
packets.sort((l, r) => { const res = goesBefore(l,r); return res === true ? -1 : res === false ? 1 : 0; });
const packets_str = packets.map((p) => JSON.stringify(p))
const key = (packets_str.indexOf('[[2]]') + 1) * (packets_str.indexOf('[[6]]') + 1)
console.log(`Part 2: What is the decoder key for the distress signal? ${key}`);
