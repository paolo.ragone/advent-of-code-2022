// https://adventofcode.com/2022/day/1

import { loadInputFile_StrByLine, splitArrayIntoSubArrays } from "../utils";

const raw_input = loadInputFile_StrByLine(__dirname, 'input.txt', false);
const raw_split_input = splitArrayIntoSubArrays(raw_input, (a: string) => a.trim().length === 0)
const input = raw_split_input.map(a => a.map((v: string) => parseInt(v, 10)))

//console.log(JSON.stringify(input));
//console.assert(input.length === 5, 'There should be 5 groups');

const elf_total = input.map(a => a.reduce((a, v) => a + v, 0));
//console.log(JSON.stringify(elf_total));

elf_total.sort((a, b) => b-a)
//console.log(JSON.stringify(elf_total));

console.log('Part 1: How many total Calories is that Elf carrying?', elf_total[0]);

console.log('Part 2: How many Calories are those Elves carrying in total?', (elf_total[0] + elf_total[1] + elf_total[2]))
