// https://adventofcode.com/2022/day/5

import { assert } from "console";
import { loadInputFile_StrByLine } from "../utils";

const sample_start = [ //Top to bottom
  ["N","Z"],
  ["D","C","M"],
  ["P"]
];

const start = [
  ["N", "W", "F", "R", "Z", "S", "M", "D"],
  ["S", "G", "Q", "P", "W"],
  ["C", "J", "N", "F", "Q", "V", "R", "W"],
  ["L", "D", "G", "C", "P", "Z", "F"],
  ["S", "P", "T"],
  ["L", "R", "W", "F", "D", "H"],
  ["C", "D", "N", "Z"],
  ["Q", "J", "S", "V", "F", "R", "N", "W"],
  ["V", "W", "Z", "G", "S", "M", "R"]
];

function deepArrayCopy<T>(arr: T[][]): T[][] {
  return arr.map((row) => row.slice());
}

class CrateState {
  public crates: string[][];
  constructor(crates: string[][], public moveAtOnce = false) {
    this.crates = deepArrayCopy(crates);
  }
  public moveCrate(howMany: number, fromCrate: number, toCrate: number) {
    const takenOut = this.crates[fromCrate-1].splice(0, howMany);
    if (!this.moveAtOnce) {
      takenOut.reverse();
    }
    this.crates[toCrate-1].unshift(...takenOut);
  }
  // The instruction is in the format "move 3 from 1 to 2"
  public processInstruction(instruction: string) {
    const [_, howMany, fromCrate, toCrate] = instruction.match(/move (\d+) from (\d+) to (\d+)/) || [];
    this.moveCrate(parseInt(howMany), parseInt(fromCrate), parseInt(toCrate));
  }
  public getTopCrates() {
    return this.crates.map(crate => crate[0]).join("");
  }
}

const raw_sample_input = loadInputFile_StrByLine(__dirname, 'sample_input.txt');
const sample_crate = new CrateState(sample_start);
raw_sample_input.forEach(instruction => sample_crate.processInstruction(instruction));
assert(sample_crate.getTopCrates() === 'CMZ', 'Wrong sample result');

const raw_input = loadInputFile_StrByLine(__dirname, 'input.txt');
const crate = new CrateState(start);
raw_input.forEach(instruction => crate.processInstruction(instruction));

console.log('Part 1: After the rearrangement procedure completes, what crate ends up on top of each stack?', crate.getTopCrates());


const sample_crate2 = new CrateState(sample_start, true);
raw_sample_input.forEach(instruction => sample_crate2.processInstruction(instruction));
assert(sample_crate2.getTopCrates() === 'MCD', 'Wrong sample result. Part 2, got ' + sample_crate2.getTopCrates());

const crate2 = new CrateState(start, true);
raw_input.forEach(instruction => crate2.processInstruction(instruction));

console.log('Part 2: After the rearrangement procedure completes, what crate ends up on top of each stack?', crate2.getTopCrates());
