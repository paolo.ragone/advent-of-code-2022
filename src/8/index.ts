// https://adventofcode.com/2022/day/8

import { assert } from "console";
import { loadInputFile_IntArrNoSpaceByLine } from "../utils";


// console.log(sample_input);

function calculateVisibleTrees(input: number[][]): boolean[][] {
    const response = input.map((row) => row.map(() => false));
    // Horizontal
    for (let y = 0; y < input.length; y++) {
        let max = -1;
        for (let x = 0; x < input[y].length; x++) {
            if (input[y][x] > max) {
                max = input[y][x];
                response[y][x] = true;
            }
        }
        max = -1;
        for (let x = input[y].length-1; x >0; x--) {
            if (input[y][x] > max) {
                max = input[y][x];
                response[y][x] = true;
            }
        }
    }
    // Vertical
    for (let x = 0; x < input[0].length; x++) {
        let max = -1;
        for (let y = 0; y < input.length; y++) {
            if (input[y][x] > max) {
                max = input[y][x];
                response[y][x] = true;
            }
        }
        max = -1;
        for (let y = input.length-1; y >0; y--) {
            if (input[y][x] > max) {
                max = input[y][x];
                response[y][x] = true;
            }
        }
    }
    return response;
}

function calculateScenicScore(input: number[][], yPos: number, xPos: number): number {
    const height = input[yPos][xPos];
    if (xPos === 0 || xPos === input[0].length-1 || yPos === 0 || yPos === input.length-1) {
        return 0;
    }
    // Horizontal
    let y = yPos + 1;
    while (y <= (input.length - 1) && input[y][xPos] < height) {
        y++;
    }
    const downViewability = y - yPos + (y <= (input.length - 1) ? 0 : -1);
    y = yPos - 1;
    while (y >= 0 && input[y][xPos] < height) {
        y--;
    }
    const upViewability = yPos - y + (y >= 0 ? 0 : -1);

    let x = xPos + 1;
    while (x <= (input[0].length - 1) && input[yPos][x] < height) {
        x++;
    }
    const rightViewability = x - xPos + (x <= (input[0].length - 1) ? 0 : -1);

    x = xPos - 1;
    while (x >= 0 && input[yPos][x] < height) {
        x--;
    }
    const leftViewability = xPos - x + ( x >= 0 ? 0 : -1);
    return rightViewability * leftViewability * downViewability * upViewability;
}

function countVisibles(input: boolean[][]): number {
    return input.reduce((prev, row) => prev + row.filter((v) => v).length, 0);
}

const sample_input = loadInputFile_IntArrNoSpaceByLine(__dirname, 'sample_input.txt');
const sample_visibleTrees = calculateVisibleTrees(sample_input);
assert(countVisibles(sample_visibleTrees) === 21, 'Wrong number of visible trees');

assert(calculateScenicScore(sample_input, 0, 0) === 0, 'Wrong score');
assert(calculateScenicScore(sample_input, 1, 2) === 4, 'Wrong score 2');
assert(calculateScenicScore(sample_input, 3, 2) === 8, 'Wrong score 2');


const input = loadInputFile_IntArrNoSpaceByLine(__dirname, 'input.txt');
const visibleTrees = calculateVisibleTrees(input);
// console.log(visibleTrees.map(r => r.map(v => v ? 1: 0).join('')).join('\n'));
console.log('Part 1: how many trees are visible from outside the grid?', countVisibles(visibleTrees));

let max = 0;
for (let y = 0; y < input.length; y++) {
    for (let x = 0; x < input[y].length; x++) {
        const score = calculateScenicScore(input, y, x);
        if (score > max) {
            max = score;
        }
    }
}
console.log('Part 2: what is the maximum scenic score?', max);