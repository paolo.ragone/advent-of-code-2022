import fs from 'fs';
import path from 'path';

export type Point = {x: number, y: number};
export class Range {
  constructor(public from: number, public to: number) { }
  get length(): number {
    return this.to - this.from + 1;
  }
  intersectsWith(other: Range) {
    return this.from <= other.to && this.to >= other.from;
  }
  neighbours(other: Range) {
    return this.from === other.to + 1 || this.to === other.from - 1;
  }
  unionWith(other: Range) {
    return new Range(Math.min(this.from, other.from), Math.max(this.to, other.to));
  }
  contains(other: Range) {
    return this.from <= other.from && this.to >= other.to;
  }
}

export function manhattanDistance(a: Point, b: Point): number {
  return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
}

export type Point3D = {
  x: number;
  y:number;
  z:number;
}

export function loadInputFile_IntByLine(dir: string, file: string): number[] {
  const allContent = fs.readFileSync(path.join(dir, file), 'utf-8');
  const lines = allContent.split('\n');
  return lines.filter(l => l.trim().length > 0).map(l => parseInt(l, 10));
}

export function splitArrayIntoSubArrays<T>(arr: T[], splitCondition: (i: T) => boolean): T[][] {
  return arr.reduce((prevs: T[][], curr: T) => {
    if (splitCondition(curr)) {
      prevs.push([]);
      return prevs;
    } else {
      prevs[prevs.length - 1].push(curr);
      return prevs;
    }
  }, [[]] as T[][])
}

export function loadInputFile_IntArrNoSpaceByLine(dir: string, file: string): number[][] {
  const allContent = fs.readFileSync(path.join(dir, file), 'utf-8');
  const lines = allContent.split('\n');
  return lines.filter(l => l.trim().length > 0).map(s => [...s].map(l => parseInt(l, 10)));
}

export function loadInputFile_StrByLine(dir: string, file: string, filterEmptyLines = true): string[] {
  const allContent = fs.readFileSync(path.join(dir, file), 'utf-8');
  const lines = allContent.split('\n');
  if (filterEmptyLines)
    return lines.filter(l => l.trim().length > 0).map(l => l.trim());
  return lines.map(l => l.trim());
}

export function loadInputFile_StrNumByLine(dir: string, file: string): {key: string, value:number}[] {
  const allContent = fs.readFileSync(path.join(dir, file), 'utf-8');
  const lines = allContent.split('\n');
  return lines.filter(l => l.trim().length > 0).map(l => l.split(' ')).map(a => ({key: a[0], value: parseInt(a[1], 10)}));
}

export function loadInputFile_PointByLine(dir: string, file: string): Point[] {
  const allContent = fs.readFileSync(path.join(dir, file), 'utf-8');
  const lines = allContent.split('\n');
  return lines.filter(l => l.trim().length > 0).map(l => l.split(',')).map(a => ({x: parseInt(a[0], 10), y: parseInt(a[1], 10)}));
}

export class Histogram {
  public counts: {[key: string]: number} = {};
  increment(val: string, inc = 1) {
    this.counts[val] = (this.counts[val] ? this.counts[val] : 0) + inc;
  }
  keysInOrder(): string[] {
    const keys = Object.keys(this.counts);
    return keys.sort((a, b) => this.counts[a] - this.counts[b]);
  }
  asPairs(): {key: string, count: number}[] {
    const resp: {key: string, count: number}[] = [];
    return Object.keys(this.counts).map(k => ({key: k, count: this.counts[k]}));
  }
  splitInHalf() {
    Object.keys(this.counts).forEach(k => this.counts[k] = this.counts[k] /2);
  }
}
export function countLetters(str: string): Histogram {
  const h = new Histogram();
  [...str].forEach(l => h.increment(l));
  return h;
}

export function arraysEqual(a: any[], b: any[]) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length !== b.length) return false;

  // If you don't care about the order of the elements inside
  // the array, you should sort both arrays here.
  // Please note that calling sort on an array will modify that array.
  // you might want to clone your array first.

  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}

function splitArrayByFunction(input: string[], fn: (s: string) => boolean): string[][] {
  const result: string[][] = [];
  let current: string[] = [];
  for (const line of input) {
      if (fn(line)) {
          result.push(current);
          current = [];
      } else {
          current.push(line);
      }
  }
  result.push(current);
  return result;
}