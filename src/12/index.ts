// https://adventofcode.com/2022/day/12

import { loadInputFile_StrByLine, Point } from "../utils";

const charA = 'a'.charCodeAt(0);

class Grid {
    public grid: number[][] = [];
    public position: Point = { x: 0, y: 0 };
    public destination: Point = { x: 0, y: 0 };

    constructor(input: string[]) {
        input.forEach((line: string, y: number) => {
            const row = [] as number[];
            line.split('').forEach((char: string, x: number) => {
                if (char === 'S') {
                    this.position = { x, y };
                    row.push(0);
                } else if (char === 'E') {
                    this.destination = { x, y };
                    row.push('z'.charCodeAt(0) - charA);
                } else {
                    row.push(char.charCodeAt(0) - charA);
                }
            });
            this.grid.push(row);
        });
    }
    public findShortestLength(): number {
        const lengths = this.grid.map(row => row.map(_ => Number.MAX_SAFE_INTEGER));
        lengths[this.destination.y][this.destination.x] = 0;
        this.visitForShortestLength(lengths, this.destination);
        return lengths[this.position.y][this.position.x];
    }
    public findShortestLengthStartingFromA(): number {
        const lengths = this.grid.map(row => row.map(_ => Number.MAX_SAFE_INTEGER));
        lengths[this.destination.y][this.destination.x] = 0;
        this.visitForShortestLength(lengths, this.destination);
        return this.grid.reduce((rowMin, row, y) => row.reduce((min, val, x) => {
            if (val === 0) {
                return Math.min(min, lengths[y][x]);
            }
            return min;
        }, rowMin), Number.MAX_SAFE_INTEGER);
    }
    private visitForShortestLength(lengths: number[][], point: Point) {
        const length = lengths[point.y][point.x];
        const neighbors = this.getNeighbors(point);
        neighbors.forEach(neighbor => {
            const neighborLength = lengths[neighbor.y][neighbor.x];
            if (neighborLength > length + 1) {
                lengths[neighbor.y][neighbor.x] = length + 1;
                this.visitForShortestLength(lengths, neighbor);
            }
        });
    }
    private getNeighbors(point: Point): Point[] {
        const neighbors = [] as Point[];
        if (point.y > 0) {
            neighbors.push({ x: point.x, y: point.y - 1 });
        }
        if (point.y < this.grid.length - 1) {
            neighbors.push({ x: point.x, y: point.y + 1 });
        }
        if (point.x > 0) {
            neighbors.push({ x: point.x - 1, y: point.y });
        }
        if (point.x < this.grid[0].length - 1) {
            neighbors.push({ x: point.x + 1, y: point.y });
        }
        const pointValue = this.grid[point.y][point.x];
        return neighbors.filter(n => {
            const neighborValue = this.grid[n.y][n.x];
            return (pointValue - neighborValue) <= 1;
        });
    }
}

const sample_input = loadInputFile_StrByLine(__dirname, 'sample_input.txt');
const sample_processor = new Grid(sample_input);
const sample_shortest_length = sample_processor.findShortestLength()
console.assert(sample_shortest_length === 31, `Expected 31, got ${sample_shortest_length}`);
console.assert(sample_processor.findShortestLengthStartingFromA() === 29, `Expected 29 as shortest path starting from a`);
// console.log(`Part 1: What is the fewest steps required to move from your current position to the location that should get the best signal? ${shortestLength}`);


const input = loadInputFile_StrByLine(__dirname, 'input.txt');
const processor = new Grid(input);
const shortest_length = processor.findShortestLength()
console.log(`Part 1: What is the fewest steps required to move from your current position to the location that should get the best signal? ${shortest_length}`);
console.log(`Part 2: What is the fewest steps required to move starting from any square with elevation a to the location that should get the best signal? ${processor.findShortestLengthStartingFromA()}`);
