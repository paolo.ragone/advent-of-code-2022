// https://adventofcode.com/2022/day/2

import { loadInputFile_StrByLine } from "../utils";

const raw_input = loadInputFile_StrByLine(__dirname, 'input.txt', false);
const raw_split_input = raw_input.filter(l => l.trim().length > 0).map(l => l.split(' ', 2))

function mapOpponent(opponent: string): number {
  const pos = ['A', 'B', 'C'].indexOf(opponent);
  if (pos < 0) throw new Error(`Invalid input: ${opponent}`);
  return pos;
}

function mapMe(me: string): number {
  const pos = ['X', 'Y', 'Z'].indexOf(me);
  if (pos < 0) throw new Error(`Invalid input: ${me}`);
  return pos;
}

function doIWin(opponent: number, me: number): boolean {
  return ((opponent + 1 ) % 3 === me);
}

function isDraw(opponent: number, me: number): boolean {
  return opponent === me;
}

function doILose(opponent: number, me: number): boolean {
  return !isDraw(opponent, me) && !doIWin(opponent, me);
}

function evaluateGame(opponentStr: string, meStr: string): number {
  const opponent = mapOpponent(opponentStr);
  const me = mapMe(meStr);
  if (doIWin(opponent, me)) {
    return 6 + me + 1;
  } else if (isDraw(opponent, me)) {
    return 3 + me + 1;
  } else {
    return me + 1;
  }
}

function figureOutPlay(opponentStr: string, outcomeStr: string): string {
  const opponent = mapOpponent(opponentStr);
  switch (outcomeStr) {
    case 'X':
      // I need to lose
      const me = opponent === 0 ? 2 : opponent - 1;
      return ['X', 'Y', 'Z'][me];
    case 'Y':
      return ['X', 'Y', 'Z'][opponent];
    default:
      return ['X', 'Y', 'Z'][(opponent + 1) % 3];
  }
}

// raw_split_input.forEach((game) => {
//   console.log('Game: ', JSON.stringify(game), ' ==> ', evaluateGame(game[0], game[1]))
// });

const outcomes = raw_split_input.map(game => evaluateGame(game[0], game[1]));
const acum = outcomes.reduce((a, v) => a+ v ,0);
console.log('Part 1: What would your total score be if everything goes exactly according to your strategy guide?', acum);


const acum2 = raw_split_input.map(strategy => {
  const opponentStr = strategy[0];
  const me = figureOutPlay(opponentStr, strategy[1]);
  const result = evaluateGame(opponentStr, me);
//  console.log('Given strategy ', strategy[0], strategy[1], ' I must play ', me, ' and that produces ', result);
  return result;
}).reduce((a, v) => a+v, 0);

console.log('Part 2: what would your total score be if everything goes exactly according to your strategy guide?', acum2);
